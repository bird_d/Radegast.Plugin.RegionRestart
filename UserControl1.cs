﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Radegast;
using OpenMetaverse;

namespace Radegast.Plugin.RestartTab
{
    [Radegast.Plugin(Name = "RestartTab", Description = "Configure Weekly restarts.", Version = "0.2")]
    public partial class UserControl1 : RadegastTabControl, IRadegastPlugin
    {
        static string tabID = "restart_config";
        static string tabLabel = "Restarter Config";
        ToolStripMenuItem ActivateTabButton;
        System.Timers.Timer timer;
        int WarningDelay;

        public UserControl1()
        {
        }

        public UserControl1(RadegastInstance instance, bool unused)
            : base(instance)
        {
            InitializeComponent();
        }
        //Init variables;
        public  void StartPlugin(RadegastInstance inst)
        {
            this.instance = inst;
            ActivateTabButton = new ToolStripMenuItem(tabLabel, null, MenuButtonClicked);
            instance.MainForm.PluginsMenu.DropDownItems.Add(ActivateTabButton);
        }

        public void StopPlugin(RadegastInstance inst)
        {
            ActivateTabButton.Dispose();
            instance.TabConsole.Tabs[tabID].Close();
            timer.Stop();
            instance.Client.Self.Chat("Region Restart is not loaded!!", 16, ChatType.Normal);
        }


        void MenuButtonClicked(object sender, EventArgs e)
        {
            if (instance.TabConsole.TabExists(tabID))
            {
                instance.TabConsole.Tabs[tabID].Select();
            }
            else
            {
                instance.TabConsole.AddTab(tabID, tabLabel, new UserControl1(instance, true));
                instance.TabConsole.Tabs[tabID].Select();
            }
        }
//

        private void ScheduleTimer()
        {
            if (timer != null) timer.Stop();
            this.WarningDelay = (int)delayUpDown.Value;
            int Minutes = ((int)hourUpDown.Value * 60) + (int)minuteUpDown.Value;
            DateTime nowTime = DateTime.Now;
            //Old
            DateTime scheduledTime = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, (int)hourUpDown.Value, (int)minuteUpDown.Value, 0, 0);
            //Correct time if hours is less than nowTime hours.
            if (nowTime > scheduledTime)
            {
                scheduledTime = scheduledTime.AddDays(1);
            }

            int timeOut = 0;
            bool found = false;
            int start = (int)scheduledTime.DayOfWeek;
            //Find the first checked day or timeout after a week.
            while (timeOut < 7 && found == false )
            {
                if (dayCheckListBox.GetItemCheckState(start % 7) == CheckState.Checked)
                {
                    found = true;
                } else
                {
                    timeOut += 1;
                    start += 1;
                }
            }
            //Skip to first checked day
            scheduledTime = scheduledTime.AddDays(start - (int)scheduledTime.DayOfWeek);
            //Correct Datetime from the warning time.
            scheduledTime = scheduledTime.AddSeconds(-WarningDelay);
            //Total amount of Milliseconds for the timer.
            double tickTime = (double)(scheduledTime - DateTime.Now).TotalMilliseconds;
            timer = new System.Timers.Timer(tickTime);
            timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsed);
            timer.Start();
            //Human readable string to send to scripted object
            string toPDT = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(scheduledTime.AddSeconds(this.WarningDelay), "Pacific Standard Time").ToString();
            dateLabel.Text = "Restart at " + toPDT + " PDT/PST";
            instance.Client.Self.Chat("Region will restart at " + toPDT + " PDT/PST, with " + this.WarningDelay + " seconds of Warning.", 16, ChatType.Normal);
        }
        private void TimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Stop();
            if(!instance.Client.Network.Connected)
            {
                ScheduleTimer();
            }
            else
            {
                //Crash the sim for free
                instance.Client.Estate.EstateOwnerMessage("restart", WarningDelay.ToString());
                ScheduleTimer();
            }
        }

        private void btnResetTimer_Click(object sender, EventArgs e)
        {
            ScheduleTimer();
        }
    }
}
